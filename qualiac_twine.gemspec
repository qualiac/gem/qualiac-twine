Gem::Specification.new do |s|
  s.name        = 'qualiac_twine'
  s.version     = '0.0.10'
  s.date        = '2019-09-12'
  s.description = s.summary = 'Qualiac plugin for twine'
  s.description += '.' # avoid identical warning
  s.authors     = ["Damien DANGLARD"]
  s.email       = 'ddanglard@cegid.com'
  s.homepage    = 'http://rubygems.org/gems/qualiac_twine'
  s.license     = 'https://gitlab.com/qualiac/gem/qualiac-twine/blob/master/LICENSE'
  s.require_paths = ['lib']

  s.add_runtime_dependency 'twine'

  s.files = `git ls-files`.split("\n")
end
