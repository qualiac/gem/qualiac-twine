require 'rubygems'
require 'twine'

require_relative 'formatter/qkr_string_swift'
require_relative 'formatter/qkr_string_swift_kernel'

Twine::Formatters.register_formatter Twine::Formatters::QKRStringSwift
Twine::Formatters.register_formatter Twine::Formatters::QKRStringSwiftKernel